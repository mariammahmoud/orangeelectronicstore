package products;
import java.util.Comparator;

import products.Type;

public class Electronic {
	String name; 
	String manufacture;
	String description;
	int price; 
	Type type; 
	int id;
	public static int countID= 1; 
	int stock;
	
	protected Electronic (String n, String m, String d, int p, int s)
	{
		name = n; 
		manufacture=m;
		description= d; 
		price = p; 
		id = countID;
		++ countID;
		stock= s;
		
	}
	
	public void setType(Type type) {
		this.type = type;
	}

	public Electronic (int ID)
	{
		this.id= ID; 
	}
	
	public void buy()
	{
		if(stock>0)
		{
		System.out.println("Congrats you bought this item.");
		--stock;
		}
		else
		System.out.println("this item is out of stock ");
	}
	
	public String toString()
	{
		String s= "Item: " + type + ", Name: " + name + ", ID: " + id + ", Manufacturer: " + manufacture + ", Price: " + price+ " ,Stock: " + stock; 
		return s; 
	}

	public int getId() {
		
		return id;
	}

	
	
	public String getName() {
		return name;
	}

	public String getManufacture() {
		return manufacture;
	}

	public int getPrice() {
		return price;
	}

	public Type getType() {
		return type;
	}

	public void setId(int id) {
		this.id = id;
	}
	public static Comparator<Electronic> c = new Comparator<Electronic>() 
	{
		public int compare(Electronic e1, Electronic e2){
			return Integer.toString(e1.getId()).compareTo(Integer.toString(e2.getId()));   
		}
	};
	

	
}

