package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;
import products.Electronic;
import products.Mobile;
import products.Tablet;
import products.Type;
import users.Admin;
import users.Customer;
import users.Person;
import users.Role;


public class Store {
	private ArrayList <Electronic> allStoreProducts;
	public ArrayList<Person> allPeople; 
	public Person currentUser;
	private static int oneStore;
	private static Store myStore;

	//constructor
	private Store()
	{
		allStoreProducts = new ArrayList<Electronic> ();
		allPeople = new ArrayList<Person> ();
		myStore= this; 
	}
	
	//create one store 
	public static Store createStore()
	{
		Store myStore=null;
		if (oneStore == 0)
		{
			oneStore = 1;
			myStore = new Store();	
			System.out.println("Store created successfully");
		}
		else
			System.out.println("One Store already created");
		return myStore;
	}
	
	//add person to arraylist
	public static void addPerson( Person p)
	{
		myStore.allPeople.add(p);
		Collections.sort(myStore.allPeople, Person.nameComparator);
	}
	
	
	//create and add product to arraylist
	public void createProduct(String n, String m, String d, int p, Type type, int s)
	{
		if ( this.currentUser.getRole()== Role.Admin)
		{
			if (type== Type.M)
			{
				Electronic mobile = new Mobile(n,m,d,p,s);
				allStoreProducts.add(mobile);
			}
			else
			{
				Electronic tablet = new Tablet(n,m,d,p,s);
				allStoreProducts.add(tablet);
			}
			System.out.println("Product added ");
		}
		else
		{
			System.out.println("Unauthorised access");
		}
	}
	
	//login and authorisation 
	public static void login()
	{
		Scanner sc = new Scanner (System.in);
		System.out.println( "Enter username: ");
		String username = sc.nextLine();
		System.out.println( "Enter password: ");
		String password = sc.nextLine();
		
    	int index= Collections.binarySearch(myStore.allPeople, new Person(username,null,null,false), Person.nameComparator);
    	if( index >= 0 )
    	{
	    	Person p= myStore.allPeople.get(index);
	    	if ( (p.getPassword().equals(password)) && myStore.currentUser==null)
	    	{
	    		System.out.println( " Login successful ");
	    		myStore.currentUser=p;
	    	}
	    	else
	    		System.out.println( "login unsuccessful");
    	}
    	else
    		System.out.println( "login unsuccessful; username not found");
 
		
	}
	public static void logout()
	{
		myStore.currentUser= null;
		System.out.println( "logged out successfully");
	}
	
	
	//print all available products
	public void viewAllProducts()
	{
		System.out.println( allStoreProducts);
	}
	
	
    //SEARCH BY METHODS 
    public void buy( int ID)
	{
    	if ( this.currentUser.getRole()== Role.Customer)
    	{
	    	
	    	int index= Collections.binarySearch(allStoreProducts, new Electronic(ID), Electronic.c);
			allStoreProducts.get(index).buy();
    	}
    	else System.out.println(" admin not allowed to buy");
		         
	}
	
	public void searchByPrice( int p)
	{
		System.out.println(" Search by price: ");
		for (Electronic e: allStoreProducts)
		{
			if(e.getPrice() <= p)
				System.out.println(e);	
		}	
	}
	
	public void searchByManuf( String m)
	{ 
		System.out.println(" Search by manufacturer: ");
		for (Electronic e: allStoreProducts)
		{
			if(e.getManufacture() == m)
				System.out.println(e);	
		}
	}
	
	public void searchByType ( Type t )
	{
		System.out.println(" Search by Type: ");
		for (Electronic e: allStoreProducts)
		{
			if(e.getType() == t)
				System.out.println(e);	
		}
	}
	
	public void searchByName( String n )
	{
		System.out.println(" Search by Name: ");
		for (Electronic e: allStoreProducts)
		{
			if(e.getName() == n)
				System.out.println(e);	
		}
	}
	// END OF SEARCH BY METHODS 
	
	
	public static void main(String[]args)
	{
		//create store
		Store myStore = createStore();
		//test only one store can be created
		//Store secondStore= createStore();
		
		//create people
		Person p1 = new Customer("username", "myemail@gmail.com", "pass1234", "credit card info");
		Person p2 = new Admin("adminname", "myemail@gmail.com", "pass1234", "manager");
		Person p3 = new Customer("mariam", "myemail@gmail.com", "pass1234", "credit card info");
		System.out.println(myStore.allPeople);
		
		//question: should login be static or not?
		//log in as admin
		login();
		myStore.createProduct("Note 9", "Samsung", "Good", 10500, Type.M,3);
		myStore.createProduct("iphone 8", "Apple", "Good", 7500, Type.M,1);
		myStore.viewAllProducts();
		myStore.buy(1);
		myStore.viewAllProducts();
		logout();
		
		
		//log in as customer
		login();
		myStore.createProduct("name2", "Samsung", "Good", 5500, Type.T,1);
		myStore.viewAllProducts();
		myStore.buy(1);
		myStore.viewAllProducts();
		
		
		/*myStore.createProduct("iphone 8", "Apple", "Good", 7500, Type.M,1);
		myStore.createProduct("Note 9", "Samsung", "Good", 10500, Type.M,3);
		myStore.createProduct("name1", "Lenovo", "Good", 2500, Type.T,2);
		myStore.createProduct("name2", "Samsung", "Good", 5500, Type.T,1);
		myStore.createProduct("ipad", "Apple", "Good", 9500, Type.T,4);
		
		myStore.viewAllProducts();
		myStore.searchByManuf("Apple");
		//myStore.searchByPrice(6000);
		//myStore.buy(1);
		//myStore.viewAllProducts();
		
		myStore.createProduct("ipad2", "Apple", "Good", 12500, Type.T,3);*/
	}
	
}

