package users;

import java.lang.reflect.Method;
import java.util.Comparator;

import main.Store;
import users.Role;

public class Person {
	int id; 
	String username; 
	String email;
	private String password;
	Role role;
	public static int countID= 1; 
	
	public Person(String n, String e, String p)
	{
		username= n; 
		email=e;
		password= p;
		id = countID;
		++ countID;
		addToStore();
		
	}
	public Person(String n, String e, String p, boolean f)
	{
		username= n; 
		email=e;
		password= p;
		id = 0;
	}
	
	public Role getRole()
	{
		return this.role; 
	}
	
	public void addToStore()
	{  
		Store.addPerson(this);	
	}
	
	public String getPassword() {
		return password;
	}

	public String toString()
	{
		return ( role + " with ID: " + id + " has username: " + username);
	}
	
	public static Comparator<Person> nameComparator = new Comparator<Person>(){         
	         
		@Override
		 public int compare(Person p1, Person p2) {             
		      return (int) (p1.username.compareTo(p2.username));         
		 }
	 };
}

