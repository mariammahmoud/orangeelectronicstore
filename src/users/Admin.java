package users;

import products.Electronic;
import products.Mobile;
import products.Tablet;
import products.Type;

public class Admin extends Person{
String title;
	
	public Admin(String n, String e, String p, String t)
	{
		super(n,e,p);
		role= Role.Admin;
		title= t;
	}
	
	public void createProduct()
	{
		
	}
	
	public void removeOutOfStock()
	{
		
	}
	
}
